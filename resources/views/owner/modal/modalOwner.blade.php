<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          @csrf
          <input type="text" id="id" hidden>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Cedula:</label>
            <input type="text" class="form-control" id="card" required>
          </div>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Nombre:</label>
            <input type="text" class="form-control" id="name" required>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Apellido:</label>
            <input type="text" class="form-control" id="last_name" required>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveClient()" id="update">Actualizar</button>
        <button type="button" class="btn btn-primary" onclick="saveClient()" id="save">Crear</button>
      </div>
    </div>
  </div>
</div>
<script>
    function dataOwner(id,card,name,last_name){
        $("#id").val(id);
        $("#card").val(card);
        $("#name").val(name);
        $("#last_name").val(last_name);

        if(id == "")
        { 
            $('#update').hide();
            $('#save').show();
        }else{
            $('#update').show();
            $('#save').hide();
        }
    }

    function saveClient(){
        var id      =   $("#id").val();
        var card    =   $("#card").val();
        var name    =   $("#name").val();
        var city_id =   $("#city_id").val();
        
        if(id == "")
        { 
          var url =  "{{url('api/owner')}}";
          var method =  "post";
          var message = "Propietario  Creado."
        }else{
          var url =  "{{url('api/owner')}}/"+id;
          var method =  "patch";
          var message = "Propietario actualizado."
        }

        $.ajax({
          headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            url:    url ,
            type:   method,
            data:   { 
                card    :card,
                name    :name,
                city_id :city_id
            },
            success : function(response, textStatus, jqXhr) {
              alert(message);
            },
            error : function(jqXHR, textStatus, errorThrown) {
              var errors = jqXHR.responseJSON.errors;
              var messErr;
              $.each(errors,function(index, value){
                messErr += "\n"+value ;
              })
              alert(messErr);
            },
        });
      }

      function deleteclient(id){  
        var r = confirm("Esta seguro de eliminar el usuario");
        if (r == true) {
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:     "{{url('api/owner')}}/"+id,
            type:   "delete",
            success : function(response, textStatus, jqXhr) {
              alert('Propietario eliminado ');
            },
          });
        } else {
          close();
        }
      }
</script>