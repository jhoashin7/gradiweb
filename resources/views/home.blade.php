@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <master-component></master-component>
    </div>
</div>
@endsection
