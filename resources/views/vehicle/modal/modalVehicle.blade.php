<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">New message</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          @csrf
          <input type="text" id="id" hidden>
          <div class="form-group">
            <label for="recipient-name" class="col-form-label">Placa:</label>
            <input type="text" class="form-control" id="plate" required>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Tipo:</label>
            <Select id="type_id" class="form-control" required>
                @foreach($types as $type)
                    <option value="{{$type->id}}">{{$type->type}}</option>
                @endforeach
            </Select>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Marca:</label>
            <Select id="brand_id" class="form-control" required>
                @foreach($brands as $brand)
                    <option value="{{$brand->id}}">{{$brand->brand}}</option>
                @endforeach
            </Select>
          </div>
          <div class="form-group">
            <label for="message-text" class="col-form-label">Propietario:</label>
            <Select id="owner_id" class="form-control" required>
                @foreach($owners as $owner)
                    <option value="{{$owner->id}}">{{$owner->card}} - {{$owner->name}}  @isset($owner->last_name){{$owner->last_name}}@endisset</option>
                @endforeach
            </Select>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" onclick="saveClient()" id="update">Actualizar</button>
        <button type="button" class="btn btn-primary" onclick="saveClient()" id="save">Crear</button>
      </div>
    </div>
  </div>
</div>
<script>
    function dataVehicle(id,plate,type_id,brand_id,owner_id){
        $("#id").val(id);
        $("#plate").val(plate);
        $("#type_id").val(type_id);
        $("#type_id").change();
        $("#brand_id").val(brand_id);
        $("#brand_id").change();
        $("#owner_id").val(owner_id);
        $("#owner_id").change();
        

        if(id == "")
        { 
            $('#update').hide();
            $('#save').show();
        }else{
            $('#update').show();
            $('#save').hide();
        }
    }

    function saveClient(){
        var id      =   $("#id").val();
        var plate   =   $("#plate").val();
        var type_id =   $("#type_id").val();
        var brand_id =   $("#brand_id").val();
        var owner_id =   $("#owner_id").val();
        
        if(id == "")
        { 
          var url =  "{{url('api/vehicle')}}";
          var method =  "post";
          var message = "Vehiculo Creado."
        }else{
          var url =  "{{url('api/vehicle')}}/"+id;
          var method =  "patch";
          var message = "Vehiculo actualizado."
        }

        $.ajax({
          headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            url:    url ,
            type:   method,
            data:   {
                plate       :plate,
                type_id     :type_id,
                brand_id    :brand_id,
                owner_id    :owner_id
            },
            success : function(response, textStatus, jqXhr) {
              alert(message);
              location.reload();
            },
            error : function(jqXHR, textStatus, errorThrown) {
              var errors = jqXHR.responseJSON.errors;
              var messErr;
              $.each(errors,function(index, value){
                messErr += "\n"+value ;
              })
              alert(messErr);
            },
        });
      }

      function deleteVehicle(id){  
        var r = confirm("Esta seguro de eliminar el vehiculo");
        if (r == true) {
          $.ajax({
            headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url:     "{{url('api/vehicle')}}/"+id,
            type:   "delete",
            success : function(response, textStatus, jqXhr) {
              alert('Vehiculo eliminado ');
              location.reload();
            },
          });
        } else {
          close();
        }
      }
</script>