@extends('layouts.app')

@section('content')
<div class="container">

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                <nav class="navbar navbar-expand-lg navbar-light bg-light">
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <span class="nav-link" ><h1>Listado de Vehiculos</h1> <span class="sr-only">(current)</span></span>
                            </li>
                        </ul>
                        <span class="btn btn-outline my-2 my-sm-0" data-toggle="modal" data-target="#exampleModal" onclick="dataVehicle('','','','')">
                        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-plus-circle" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z"/>
                            <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z"/>
                            <path fill-rule="evenodd" d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"/>
                        </svg>
                        </span>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" onclick="groupPerTime()">Agrupar por huso horario</button>
                        </div>
                    </div>
                </nav>
                </div>

                <div class="card-body">
                <table id="vehicles" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th scope="col">Id</th>
                            <th scope="col">Placa</th>
                            <th scope="col">Marca</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Propietario</th>
                            <th scope="col">Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                    Filtrar por marca:
                    <select class="form-control form-control-sm" onchange="changeBrand(this.value)" id="brandFilter">
                        <option value="all">Todas las marcas </option>
                        @foreach($brands as $brand)    
                            <option value="{{$brand->id}}">{{$brand->brand}}</option>
                        @endforeach
                    </select>
                        @foreach($vehicles as $vehicle)
                            <tr>
                                <td>{{$vehicle->id}}</td>
                                <td>{{$vehicle->plate}}</td>
                                <td>{{$vehicle->brand}}</td>
                                <td>{{$vehicle->type}}</td>
                                <td>{{$vehicle->card.' - '.$vehicle->name.' '.$vehicle->last_name}}</td>
                                <td>
                                    <span title="Actualizar" style="cursor: pointer;" data-toggle="modal" data-target="#exampleModal" onclick='dataVehicle("{{$vehicle->id}}","{{$vehicle->plate}}","{{$vehicle->type_id}}","{{$vehicle->brand_id}}","{{$vehicle->owner_id}}")'>
                                        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-pencil-square" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                        </svg>
                                    </span>
                                    <span title="Eliminar" style="cursor: pointer;" onclick='deleteVehicle("{{$vehicle->id}}")'>
                                        <svg width="2em" height="2em" viewBox="0 0 16 16" class="bi bi-trash2" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M3.18 4l1.528 9.164a1 1 0 0 0 .986.836h4.612a1 1 0 0 0 .986-.836L12.82 4H3.18zm.541 9.329A2 2 0 0 0 5.694 15h4.612a2 2 0 0 0 1.973-1.671L14 3H2l1.721 10.329z"/>
                                            <path d="M14 3c0 1.105-2.686 2-6 2s-6-.895-6-2 2.686-2 6-2 6 .895 6 2z"/>
                                            <path fill-rule="evenodd" d="M12.9 3c-.18-.14-.497-.307-.974-.466C10.967 2.214 9.58 2 8 2s-2.968.215-3.926.534c-.477.16-.795.327-.975.466.18.14.498.307.975.466C5.032 3.786 6.42 4 8 4s2.967-.215 3.926-.534c.477-.16.795-.327.975-.466zM8 5c3.314 0 6-.895 6-2s-2.686-2-6-2-6 .895-6 2 2.686 2 6 2z"/>
                                        </svg>
                                    </span>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
        $('#vehicles').DataTable();
        $('#brandFilter').val("{{$currentBrand}}")
        
    } );
    function changeBrand(val){
        location.href ="{{url('api/vehicleList/brand')}}/"+val
    }
    
    function groupPerTime(){
        var dates=  [
                        ["2018-12-01","AM","ID123",5000],
                        ["2018-12-01","AM","ID545",4000],
                        ["2018-12-01","AM","ID545",6000],
                        ["2018-12-01","AM","ID545",3000],
                        ["2018-12-01","AM","ID545",2000],
                        ["2018-12-01","PM","ID545",1000],
                        ["2018-12-02","AM","ID545",8000],
                        ["2018-12-02","PM","ID545",8000],
                        ["2018-12-03","AM","ID545",8000],
                        ["2018-12-04","AM","ID545",8000],
                        ["2018-12-05","AM","ID545",8000],
                        ["2018-12-05","PM","ID545",8000],
                        ["2018-12-05","PM","ID545",8000],
                    ]
        var arraydate = [];
        var timeZone  = "";
        var valueKey  = "";
            // se recorre el arreglo de fatos para crear un arreglo unificado por cada fecha u huso horario 
        $.each(dates,function( key, value ){
            if(value[0]!=timeZone[0]){
                valueKey += value[0]+'&'+value[1]+',';
                arraydate[value[0]+'&'+value[1]] = 0;
                arraydate[value[0]+'&'+value[1]] += value[3];
            }else{
                if (value[1] == timeZone[1]){
                    arraydate[value[0]+'&'+value[1]] += value[3];
                }else{
                    valueKey += value[0]+'&'+value[1]+',';
                    arraydate[value[0]+'&'+value[1]] = 0;
                    arraydate[value[0]+'&'+value[1]] += value[3];
                }
            }
            timeZone = value;
        });

        valueKey = valueKey.split(","); //
        var valueOld = "";
        var huso = "";
        var object = "{";

        //Se parsea  a json 
        $.each(valueKey,function( key, value ){
            var separator = value.split("&")
            if(typeof separator[0]  != "undefined" && separator[0] != ""){
                if(separator[0] != huso){
                    if(key>0)
                        object += '},'
                    object +='"'+separator[0]+'":{"'+separator[1]+'":'+arraydate[value];
                }else{
                    object +=',"'+separator[1]+'":'+arraydate[value];
                }
            }

            huso = separator[0];
        });
        object += "}}";
        console.log(JSON.parse(object));
    }
</script>

@include('vehicle.modal.modalVehicle')
@endsection
