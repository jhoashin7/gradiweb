<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            'brand'=>'Audi',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Honda',
            'created_at'=>DB::raw('now()')
        ]); 

        DB::table('brands')->insert([
            'brand'=>'Ford',
            'created_at'=>DB::raw('now()')
        ]); 

        DB::table('brands')->insert([
            'brand'=>'Tesla',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Kia',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Mercedes-Benz',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Lamborghini',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Nissan',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Mazda',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Toyota',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Chevrolet',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Mitsubishi',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Suzuki',
            'created_at'=>DB::raw('now()')
        ]); 

        DB::table('brands')->insert([
            'brand'=>'Volvo',
            'created_at'=>DB::raw('now()')
        ]); 

        DB::table('brands')->insert([
            'brand'=>'Renault',
            'created_at'=>DB::raw('now()')
        ]); 

        DB::table('brands')->insert([
            'brand'=>'Jeep',
            'created_at'=>DB::raw('now()')
        ]); 

        DB::table('brands')->insert([
            'brand'=>'Subaru',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'BMW',
            'created_at'=>DB::raw('now()')
        ]); 
        DB::table('brands')->insert([
            'brand'=>'Ferrari',
            'created_at'=>DB::raw('now()')
        ]); 
    }
}
