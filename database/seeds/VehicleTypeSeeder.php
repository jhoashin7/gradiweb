<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class VehicleTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('vehicle_types')->insert([
            'type'=>'Moto',
            'created_at'=>DB::raw('now()')           
        ]); 
        DB::table('vehicle_types')->insert([
            'type'=>'Carro',
            'created_at'=>DB::raw('now()')
        ]); 

        DB::table('vehicle_types')->insert([
            'type'=>'Camión',
            'created_at'=>DB::raw('now()')
        ]); 

        DB::table('vehicle_types')->insert([
            'type'=>'Bus',
            'created_at'=>DB::raw('now()')
        ]); 
    }
}
