<?php

namespace Tests\Feature;

use App\Models\Brand;
use App\Models\Owner;
use App\Models\Vehicle;
use App\Models\VehicleType as ModelsVehicleType;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use VehicleType;

class VehicleTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use WithFaker;
    public function testStore()
    {
        $owner  = Owner::all()->random(1);
        $type   = ModelsVehicleType::all()->random(1);
        $brand  = Brand::all()->random(1);
        $response = $this->post('api/vehicle',
        [
            'plate'     =>  "Jdd45c",
            'owner_id'  =>  $owner[0]->id,
            'type_id'   =>  $type[0]->id,
            'brand_id'  =>  $brand[0]->id
        ]);
        //$response->dumpHeaders();
        $response->assertStatus(201);
    }

    public function testAll()
    {
        $response = $this->get('api/vehicle');
        //$response->dumpHeaders();
        $response->assertStatus(200);
    }

    public function testShow()
    {
        $owner = Vehicle::all()->random(1);
        $response = $this->get('api/vehicle/'.$owner[0]->id);
        //$response->dumpHeaders();
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $owner  = Owner::all()->random(1);
        $type   = ModelsVehicleType::all()->random(1);
        $brand  = Brand::all()->random(1);
        $vehicle= Vehicle::all()->random(1);
        $response = $this->patch('api/vehicle/'.$vehicle[0]->id,
        [
            'plate'     =>  "Jdd46c",
            'owner_id'  =>  $owner[0]->id,
            'type_id'   =>  $type[0]->id,
            'brand_id'  =>  $brand[0]->id
        ]);
        $response->assertStatus(200);
    }

    public function testDelete()
    {
        $vehicle= Vehicle::all()->random(1);
        $response = $this->delete('api/vehicle/'.$vehicle[0]->id);
        //$response->dumpHeaders();
        $response->assertStatus(200);
    }

    public function testAdvance()
    {
        $type   = ModelsVehicleType::all()->random(1);
        $brand  = Brand::all()->random(1);
        $response = $this->get('api/vehicleAdvance/brand/'.$brand[0]->id.'/type/'.$type[0]->id);
        //$response->dumpHeaders();
        $response->assertStatus(200);
    }
}
