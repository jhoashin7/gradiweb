<?php

namespace Tests\Feature;

use App\Models\Owner;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OwnerTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use WithFaker;
    public function testStore()
    {
        $response = $this->post('api/owner',
        [
            'card'      => $email = $this->faker->unique()->numberBetween(10000000,999999999999),
            'name'      =>$this->faker->name()
        ]);
        //$response->dumpHeaders();
        $response->assertStatus(201);
    }

    public function testAll()
    {
        $response = $this->get('api/owner');
        //$response->dumpHeaders();
        $response->assertStatus(200);
    }

    public function testShow()
    {
        $owner = Owner::all()->random(1);
        $response = $this->get('api/owner/'.$owner[0]->id);
        //$response->dumpHeaders();
        $response->assertStatus(200);
    }

    public function testUpdate()
    {
        $owner = Owner::all()->random(1);        
        $response = $this->patch('api/owner/'.$owner[0]->id,
        [
            'card'      =>  $email = $this->faker->unique()->numberBetween(10000000,999999999999),
            'name'      =>  $this->faker->firstName,
            'last_name' =>  $this->faker->lastName
        ]);
        $response->assertStatus(200);
    }

    public function testDelete()
    {
        $owner = Owner::all()->random(1);
        $response = $this->delete('api/owner/'.$owner[0]->id);
        //$response->dumpHeaders();
        $response->assertStatus(200);
    }

}
