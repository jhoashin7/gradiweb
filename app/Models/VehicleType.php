<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class VehicleType
 * 
 * @property int $id
 * @property string $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|Vehicle[] $vehicles
 *
 * @package App\Models
 */
class VehicleType extends Model
{
	protected $table = 'vehicle_types';

	protected $fillable = [
		'type'
	];

	public function vehicles()
	{
		return $this->hasMany(Vehicle::class, 'type_id');
	}
}
