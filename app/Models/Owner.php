<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Owner
 * 
 * @property int $id
 * @property string $card
 * @property string $name
 * @property string $last_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|Vehicle[] $vehicles
 *
 * @package App\Models
 */
class Owner extends Model
{
	protected $table = 'owners';

	protected $fillable = [
		'card',
		'name',
		'last_name'
	];

	public function vehicles()
	{
		return $this->hasMany(Vehicle::class);
	}
}
