<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class Vehicle
 * 
 * @property int $id
 * @property string $plate
 * @property int $type_id
 * @property int $brand_id
 * @property int $owner_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Brand $brand
 * @property Owner $owner
 * @property VehicleType $vehicle_type
 *
 * @package App\Models
 */
class Vehicle extends Model
{
	protected $table = 'vehicles';

	protected $casts = [
		'type_id' => 'int',
		'brand_id' => 'int',
		'owner_id' => 'int'
	];

	protected $fillable = [
		'plate',
		'type_id',
		'brand_id',
		'owner_id'
	];

	public function brand()
	{
		return $this->belongsTo(Brand::class);
	}

	public function owner()
	{
		return $this->belongsTo(Owner::class);
	}

	public function vehicle_type()
	{
		return $this->belongsTo(VehicleType::class, 'type_id');
	}

	public static function vehicleAdvance($brand_id = null)
	{
		$where = " vehicles.id is not null";
		if($brand_id != "all")
			$where .= " and brand_id = $brand_id";

		return 
			Vehicle::join('owners','owners.id','=','vehicles.owner_id')
				->join('vehicle_types','vehicle_types.id','=','vehicles.type_id')
				->join('brands','brands.id','=','vehicles.brand_id')
				->select(DB::raw('vehicles.id,brand_id,brand,type_id,type,owner_id,name,last_name,plate,card'))
				->whereRaw($where)
				->get();
	}
}
