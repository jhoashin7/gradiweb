<?php 
namespace App\Services\Vehicle;

use App\Models\Brand;
use App\Models\Owner;
use App\Models\Vehicle;
use App\Models\VehicleType;
use App\Services\Vehicle\IVehicle;
use Illuminate\Support\Facades\DB;

class VehicleService implements IVehicle{
    public function vehicleList($brand_id)
    {
        $response['vehicles']   =  Vehicle::vehicleAdvance($brand_id);
        $response['owners']     =  Owner::all();
        $response['brands']     =  Brand::all();
        $response['types']      =  VehicleType::all();
        return $response;
    }
    public function vehicleStore($data)
    {
        try {
            DB::beginTransaction();
                $response = Vehicle::create($data);
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            $response = "Error sql";
        }
        return $response;
    }

    public function vehicleShow($vehicle_id)
    {
        return Vehicle::find($vehicle_id);
    }
    public function vehicleUpdate($data, $vehicle_id)
    {
        Vehicle::where('id',$vehicle_id)
            ->update($data);

        return Vehicle::find($vehicle_id);
    }

    public function vehicleDelete($vehicle_id)
    {
        return Vehicle::where('id',$vehicle_id)
                     ->delete();
    }
}