<?php

namespace App\Services\Vehicle;

interface IVehicle {
    public function vehicleList($brand_id); 
    public function vehicleStore($data); 
    public function vehicleUpdate($data,$woner_id); 
    public function vehicleDelete($owner_id);
    public function vehicleShow($owner_id);

}