<?php

namespace App\Services\Owner;

interface IOwner {
    public function ownerList(); 
    public function ownerStore($data); 
    public function ownerUpdate($data,$woner_id); 
    public function ownerDelete($owner_id);
    public function ownerShow($owner_id);
}