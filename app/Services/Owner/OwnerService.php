<?php 
namespace App\Services\Owner;
use App\Models\Owner;
use Illuminate\Support\Facades\DB;

class OwnerService implements IOwner{
    public function ownerList()
    {
        return Owner::all();
    }
    public function ownerStore($data)
    {
        try {
            DB::beginTransaction();
            $response = Owner::create($data);
            DB::commit();
        } catch (\PDOException $e) {
            DB::rollBack();
            $response = "Error sql";
        }
        return $response;
    }

    public function ownerShow($owner_id)
    {
        return Owner::find($owner_id);
    }
    public function ownerUpdate($data, $owner_id)
    {
        Owner::where('id',$owner_id)
             ->update($data);

        return Owner::find($owner_id);
    }

    public function ownerDelete($owner_id)
    {
        return Owner::where('id',$owner_id)
                     ->delete();
    }
}