<?php

namespace App\Providers;

use App\Services\Owner\IOwner;
use App\Services\Owner\OwnerService;
use App\Services\Vehicle\IVehicle;
use App\Services\Vehicle\VehicleService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(IOwner::class,
                        OwnerService::class);
        $this->app->bind(IVehicle::class,
                        VehicleService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
