<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateVehicleRequest;
use App\Models\Vehicle;
use App\Services\Vehicle\IVehicle;
use Illuminate\Support\Facades\DB;

class VehicleController extends Controller
{
    private $_iVehicle;
    public function __construct(IVehicle $iVehicle)
    {
        $this->_iVehicle = $iVehicle;
    }

    public function vehicleList($brand_id)
    {
        $response = $this->_iVehicle->vehicleList($brand_id);
        return view('vehicle/vehicle',["vehicles"    =>  $response['vehicles'],
                                       "brands"      =>  $response['brands'],
                                       "types"       =>  $response['types'],
                                       "owners"      =>  $response['owners'],
                                       "currentBrand"=>  $brand_id]);
    }

    public function store(CreateVehicleRequest $request)
    {
        return $this->_iVehicle->vehicleStore($request->all());
    }

    public function show($id)
    {
        return $this->_iVehicle->vehicleShow($id);
    }

    public function update(Request $request, $id)
    {
        return $this->_iVehicle->vehicleUpdate($request->all(),$id);
    }

    public function destroy($id)
    {
        return $this->_iVehicle->vehicleDelete($id);
    }
}
