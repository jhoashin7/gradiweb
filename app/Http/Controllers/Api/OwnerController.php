<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateOwnerRequest;
use App\Models\Owner;
use App\Services\Owner\IOwner;
use Illuminate\Support\Facades\DB;

class OwnerController extends Controller
{
    private $_iOwner;

    public function __construct(IOwner $iOwner)
    {
        $this->_iOwner = $iOwner;
    }

    public function index()
    {
        $ownerList = $this->_iOwner->ownerList();
        return view('owner/owner',["owners" => $ownerList]);
    }

    public function store(CreateOwnerRequest $request)
    {
        return $this->_iOwner->ownerStore($request->all());
    }

    public function show($id)
    {
        return $this->_iOwner->ownerShow($id);
    }

    public function update(Request $request, $id)
    {
        return $this->_iOwner->ownerUpdate($request->all(),$id);
    }

    public function destroy($id)
    {
        return $this->_iOwner->ownerDelete($id);
    }
}
