<?php

use Illuminate\Http\Request;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/owner', 'Api\OwnerController')->middleware('cors');
Route::resource('/vehicle', 'Api\VehicleController')->middleware('cors');
Route::get('/vehicleList/brand/{brand_id}', 'Api\VehicleController@vehicleList')->middleware('cors');

